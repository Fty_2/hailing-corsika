/*
 * (c) Copyright 2020 CORSIKA Project, corsika-project@lists.kit.edu
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3). See file LICENSE for a full version of
 * the license.
 */

#pragma once

#include <corsika/framework/process/ContinuousProcess.hpp>
#include <corsika/modules/writers/TrackWriterParquet.hpp>
#include <corsika/framework/core/ParticleProperties.hpp>
#include <corsika/modules/writers/WriterOff.hpp>
#include <corsika/framework/geometry/Point.hpp>
#include <corsika/framework/core/PhysicalUnits.hpp>
#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>

#include <limits>
#include <string>
#include <cxxabi.h>
#include <corsika/framework/geometry/Cylinder.hpp>
#include "constants.h"


using namespace std;

namespace corsika {

  /**
   * @ingroup Modules
   * @{
   *
   * To write 3D track data to disk.
   *
   * Since the only sole purpose of this module is to generate track
   * output on disk, the default output mode is not "WriterOff" but
   * directly TrackWriterParquet. It can of course be changed.
   *
   * @tparam TOutput with default TrackWriterParquet
   */

  template <typename TTracking, typename TOutput = ObservationVolumeWriterParquet>
  class hailingProcess : public ContinuousProcess<hailingProcess<TTracking, TOutput>>, public TOutput {
  
  using TOutput::write;
  
  public:
    /**
     * Construct a new hailingProcess object.
     *
     * @param center The center of hailing array.
     * @param radius The radius of hailing array.
     * @param absorbing Flag to make the plane absorbing.
     */
    explicit hailingProcess(CoordinateSystemPtr const &cs, bool const absorbing = true);

    template <typename TParticle, typename TTrack>
    ProcessReturn doContinuous(TParticle const&, TTrack const&, bool const limitFlag);

    template <typename TParticle, typename TTrack>
    LengthType getMaxStepLength(TParticle const&, TTrack const&);

    YAML::Node getConfig() const;

    TTree* getOutputTree(){
      return outputTree_;
    }

    void setShowerId(int showerId){
        showerId_ = showerId;
    }

  private:
    //Detector geometry setting
    Point* center_;
    LengthType radius_;
    bool const deleteOnHit_;
    CoordinateSystemPtr rootCS_;
    CoordinateSystemPtr detectorCS_;
    Cylinder* cylinder_;

    //output setting
    TTree* outputTree_;
    int showerId_;
    int PDG_;
    double x_, y_, z_;
    double px_, py_, pz_;
    double energy_;
    double t_;
  };

  //! @}

} // namespace corsika

#include "../src/hailingProcess.inl"
