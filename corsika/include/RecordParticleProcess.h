//
// Created by cen on 22-7-7.
//

//#ifndef CORSIKA8_EXAMPLES_RECORDPARTICLEPROCESS_H
//#define CORSIKA8_EXAMPLES_RECORDPARTICLEPROCESS_H
#pragma once

#include <corsika/framework/process/ContinuousProcess.hpp>
#include <corsika/framework/core/ParticleProperties.hpp>
#include <corsika/modules/writers/WriterOff.hpp>
#include <corsika/framework/geometry/Point.hpp>
#include <corsika/framework/core/PhysicalUnits.hpp>
#include <corsika/framework/geometry/PhysicalGeometry.hpp>
#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>

#include "MyParticleTree.h"

namespace corsika {
    template <typename TOutput = WriterOff>
    class RecordParticleProcess : public ContinuousProcess<RecordParticleProcess<TOutput>>, public TOutput{
    public:
        RecordParticleProcess(MyParticleTree* particleTree);

        template <typename TParticle, typename TTrack>
        LengthType getMaxStepLength(TParticle const&, TTrack const&);

        template <typename TParticle, typename TTrack>
        ProcessReturn doContinuous(TParticle const&, TTrack const&, bool const limitFlag);

        void addRecordedParticle(Code beamCode){    recordedParticleList_.emplace_back(beamCode);    }
        std::vector<PDGCode> getRecordedParticle(){    return recordedParticleList_;    }
        YAML::Node getConfig() const;

    private:
        void initializeParticleList();

        std::vector<PDGCode> recordedParticleList_;
        MyParticleTree* particleTree_;

        double startE_, endE_;
        bool startFlag_;

    };

} // namespace corsika

#include "../src/RecordParticleProcess.cpp"

//#endif //CORSIKA8_EXAMPLES_RECORDPARTICLEPROCESS_H
