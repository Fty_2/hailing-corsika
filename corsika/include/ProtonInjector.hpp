#pragma once

#include <corsika/framework/core/ParticleProperties.hpp>
#include <corsika/framework/core/PhysicalUnits.hpp>
#include <corsika/framework/geometry/PhysicalGeometry.hpp>
#include <corsika/framework/geometry/Point.hpp>
#include "constants.h"

namespace corsika {
using InvEnergyType = decltype(1 / 1_GeV);

class ProtonInjector {
private:
      //
public:
  ProtonInjector(const HEPEnergyType E0,
                          double const thetaRad, double const phiRad); 
  ~ProtonInjector() {}
  template <typename TStack, typename TEnvironment>
  void SetupInjection(TStack &stack, TEnvironment &env);

  HEPEnergyType E0_;
  double thetaRad_;
  double phiRad_;

};
} // namespace corsika
#include "../src/ProtonInjector.inl"