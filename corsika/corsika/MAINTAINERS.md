
Maintainers of the CORSIKA8 project are collaborators actively taking
care of code contributions, quality control, development, and related
discussions. 

General and infrastructure:
- Ralf Ulrich <ralf.ulrich@kit.edu>, KIT
- Maximilian Reininghaus <maximilian.reininghaus@kit.edu>, KIT
- Antonio Augusto Alves Junior <antonio.junior@kit.edu>, KIT

High performance, GPU: 
- Dominik Baack <dominik.baack@tu-dortmund.de>, Dortmund
- Antonio Augusto Alves Junior <antonio.junior@kit.edu>, KIT
- Luisa Arrabito <arrabito@in2p3.fr>, Montpellier

Electromagnetic models: 
- Jean-Marco Alameddine <jean-marco.alameddine@udo.edu>, Dortmund
- Jan Soedingrekso <jan.soedingrekso@tu-dortmund.de>, Dortmund
- Maximilian Sackel <maximilian.sackel@udo.edu>, Dortmund

Hadron models:
- Felix Riehn <friehn@lip.pt>, Santiago/Lisbon
- Anatoli Fedynitch <anatoli.fedynitch@icecube.wisc.edu> ICRR Tokyo

Output formats and infrastructure:
- Remy Prechelt <prechelt@hawaii.edu>, UHM
- Ralf Ulrich <ralf.ulrich@kit.edu>, KIT

Python library:
- Remy Prechelt <prechelt@hawaii.edu>, UHM

Radio:
- Remy Prechelt <prechelt@hawaii.edu> 
- Tim Huege <tim.huege@kit.edu>, KIT

Testing, containers:
- Lukas Nellen <lukas@nucleares.unam.mx>

