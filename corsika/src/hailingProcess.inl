/*
 * (c) Copyright 2020 CORSIKA Project, corsika-project@lists.kit.edu
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3). See file LICENSE for a full version of
 * the license.
 */

const string GetClearName(const char* name){ 

    int status = -1; 

    char* clearName = abi::__cxa_demangle(name, NULL, NULL, &status); 

    const char* const demangledName = (status==0) ? clearName : name; 

    string ret_val(demangledName); 

    free(clearName); 

    return ret_val; 

} 

namespace corsika {
    template <typename TTracking, typename TOutput>
    hailingProcess<TTracking, TOutput>::hailingProcess(CoordinateSystemPtr const &cs, bool const absorbing)
    : rootCS_(cs),
      showerId_(0),
      deleteOnHit_(absorbing){
        center_ = new Point(rootCS_, {0_m, 0_m, constants::EarthRadius::Mean - detector_dz/2});
        detectorCS_ =  make_translation(rootCS_, center_->getCoordinates());
        cylinder_ = new Cylinder(detectorCS_, detector_radius, detector_dz/2);

        outputTree_ = new TTree("events","events");
        outputTree_->Branch("showerId",&showerId_,"showerId/I");
        outputTree_->Branch("PDG",&PDG_,"PDG/I");
        outputTree_->Branch("X",&x_,"X/D");
        outputTree_->Branch("Y",&y_,"Y/D");
        outputTree_->Branch("Z",&z_,"Z/D");
        outputTree_->Branch("Time",&t_,"Time/D");
        outputTree_->Branch("Energy",&energy_,"Energy/D");
        outputTree_->Branch("Px",&px_,"Px/D");
        outputTree_->Branch("Py",&py_,"Py/D");
        outputTree_->Branch("Pz",&pz_,"Pz/D");

    }







    template <typename TTracking, typename TOutput>
    template <typename TParticle, typename TTrack>
    inline ProcessReturn hailingProcess<TTracking, TOutput>::doContinuous(TParticle const& particle,
                                                            TTrack const& step, bool const stepLimit) {
        /*
        The current step did not yet reach the hailingArray, do nothing now and wait:
        */

        if (!stepLimit) {
        // @todo this is actually needed to fix small instabilities of the leap-frog
        // tracking: Note, this is NOT a general solution and should be clearly revised with
        // a more robust tracking. #ifdef DEBUG
        if (deleteOnHit_) {
            // since this is basically a bug, it cannot be tested LCOV_EXCL_START
            LengthType const check =
                (particle.getPosition() - *center_).getNorm();
            if (check < 0_m) {
            CORSIKA_LOG_WARN("PARTICLE AVOIDED OBSERVATIONPLANE {}", check);
            CORSIKA_LOG_WARN("Temporary fix: write and remove particle.");
            } else
            return ProcessReturn::Ok;
            // LCOV_EXCL_STOP
        } else
            // #endif
            return ProcessReturn::Ok;
        }

        HEPEnergyType const energy = particle.getEnergy();
        Point const pointOfIntersection = step.getPosition(1);
        // Vector const displacement = pointOfIntersection - plane_.getCenter();


        CORSIKA_LOG_TRACE("Particle detected absorbed={}", deleteOnHit_);
        
        PDG_ = static_cast<int>(particle.getPDG());
        x_ = pointOfIntersection.getCoordinates().getX() / 1_m;
        y_ = pointOfIntersection.getCoordinates().getY() / 1_m;
        z_ = pointOfIntersection.getCoordinates().getZ() / 1_m;

        DirectionVector drct = step.getDirection(1);
        auto const P = calculate_momentum(particle.getEnergy(), particle.getMass());
        px_ = (P * drct).getComponents().getX() / 1_GeV;
        py_ = (P * drct).getComponents().getY() / 1_GeV;
        pz_ = (P * drct).getComponents().getZ() / 1_GeV;
        t_ = (particle.getTime() + step.getDuration()) / 1_ms;
        energy_ = particle.getEnergy() / 1_GeV;
        outputTree_ -> Fill();
        std::cout << "Particle detected. "
                  << particle.getPID() << ". Energy: " << energy
                  << ". X: " << pointOfIntersection.getCoordinates().getX()
                  << ". Y: " << pointOfIntersection.getCoordinates().getY()
                  << ". Z: " << pointOfIntersection.getCoordinates().getZ()
                  << ". T: " << particle.getTime()
                  //                << ". Pz: " << particle.getMomentum().getComponents().getZ()
                  //               << ". Distance to center: " << (pointOfIntersection - *center_).getNorm()
                  << ". Distance to center: " << sqrt(x_*x_ + y_*y_);
                  if((z_ - (constants::EarthRadius::Mean)/1_m)==0) std::cout<<". At upper surface";
                  if( (z_ - (constants::EarthRadius::Mean - 2 * cylinder_->getZ())/1_m) == 0) std::cout<<". At lower surface";
                  std::cout << std::endl;

        // add our particles to the output file stream
        DirectionVector const dirction = particle.getDirection();
        this->write(particle.getPID(), particle.getEnergy(), pointOfIntersection.getX(detectorCS_),
                pointOfIntersection.getY(detectorCS_), pointOfIntersection.getZ(detectorCS_),
                dirction.getX(detectorCS_), dirction.getY(detectorCS_), dirction.getZ(detectorCS_),
                particle.getTime());

        if (deleteOnHit_) {
            return ProcessReturn::ParticleAbsorbed;
        } else {
            return ProcessReturn::Ok;
        }
    }



    template <typename TTracking, typename TOutput>
    template <typename TParticle, typename TTrack>
    inline LengthType hailingProcess<TTracking, TOutput>::getMaxStepLength(TParticle const& particle,
                                                            TTrack const& trajectory) {
//        sphere_->getVolume();
//        auto const intersection = TTracking::intersect(particle, *sphere_);
        auto const intersection = TTracking::intersect(particle, *cylinder_);

        TimeType const timeOfIntersection = intersection.getEntry();
        CORSIKA_LOG_TRACE("timeOfIntersection={}", timeOfIntersection);
        if (timeOfIntersection < TimeType::zero()) {
            return std::numeric_limits<double>::infinity() * 1_m;
        }
        if (timeOfIntersection > trajectory.getDuration()) {
            return std::numeric_limits<double>::infinity() * 1_m;
        }
        double const fractionOfIntersection = timeOfIntersection / trajectory.getDuration();
        CORSIKA_LOG_TRACE("ObservationPlane: getMaxStepLength dist={} m, pos={}",
                        trajectory.getLength(fractionOfIntersection) / 1_m,
                        trajectory.getPosition(fractionOfIntersection));
        return trajectory.getLength(fractionOfIntersection);
            // return meter * std::numeric_limits<double>::infinity();
    }




    template <typename TTracking, typename TOutput>
    YAML::Node hailingProcess<TTracking, TOutput>::getConfig() const {
        using namespace units::si;

        YAML::Node node;

        // // add default units for values
        // node["type"] = "hailingProcess";
        // node["units"] = "GeV | m | ns";

        return node;
    }
} // namespace corsika
