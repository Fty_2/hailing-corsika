/*
 * (c) Copyright 2020 CORSIKA Project, corsika-project@lists.kit.edu
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3). See file LICENSE for a full version of
 * the license.
 */

#pragma once

#include <corsika/framework/core/PhysicalUnits.hpp>
#include <corsika/framework/geometry/Line.hpp>
#include <corsika/framework/geometry/Point.hpp>
#include <corsika/framework/geometry/PhysicalGeometry.hpp>

namespace corsika {

  inline VelocityVector StraightTrajectory::getVelocity(double const u) const {
    return initialVelocity_ * (1 - u) + finalVelocity_ * u;
  }

  inline TimeType StraightTrajectory::getDuration(double const u) const {
    return u * timeStep_;
  }

  template <typename Particle>
  inline TimeType StraightTrajectory::getTime(Particle const& particle,
                                              double const u) const {
    return particle.getTime() + getDuration(u); // timeStep_ * u;
  }

  inline LengthType StraightTrajectory::getLength(double const u) const {
    if (timeLength_ == 0_s) return 0_m;
    if (timeStep_ == std::numeric_limits<TimeType::value_type>::infinity() * 1_s)
      return std::numeric_limits<LengthType::value_type>::infinity() * 1_m;
    return getDistance(u) * timeStep_ / timeLength_;
  }

  inline void StraightTrajectory::setLength(LengthType const limit) {
    setDuration(line_.getTimeFromArclength(limit));
  }

  inline void StraightTrajectory::setDuration(TimeType const limit) {
    if (timeStep_ == 0_s) {
      timeLength_ = 0_s;
      setFinalVelocity(getVelocity(0));
      timeStep_ = limit;
    } else {
      // for infinite steps there can't be a difference between
      // curved and straight trajectory, this is fundamentally
      // undefined: assume they are the same (which, i.e. is always correct for a
      // straight line trajectory).
      //
      // Final note: only straight-line trajectories should have
      // infinite steps! Everything else is ill-defined.
      if (timeStep_ == std::numeric_limits<TimeType::value_type>::infinity() * 1_s ||
          timeLength_ == std::numeric_limits<TimeType::value_type>::infinity() * 1_s) {
        timeLength_ = limit;
        timeStep_ = limit;
        // ...and don't touch velocity
      } else {
        const double scale = limit / timeStep_;
        timeLength_ *= scale;
        setFinalVelocity(getVelocity(scale));
        timeStep_ = limit;
      }
    }
  }

  inline LengthType StraightTrajectory::getDistance(double const u) const {
    assert(u <= 1);
    assert(u >= 0);
    return line_.getArcLength(0 * second, u * timeLength_);
  }

  inline std::pair<std::shared_ptr<BaseTrajectory>, std::shared_ptr<BaseTrajectory>>
  StraightTrajectory::getSubTrajectory(double const u) const {
    auto positionMid = getPosition(u);
    auto velocityMid = getVelocity(u);
    auto traj0 = std::make_shared<StraightTrajectory>(
        line_, timeLength_ * u, timeStep_ * u, initialVelocity_, velocityMid);
    auto traj1 = std::make_shared<StraightTrajectory>(
        Line(positionMid, velocityMid), timeLength_ * (1 - u), timeStep_ * (1 - u),
        velocityMid, finalVelocity_);
    return std::make_pair(std::move(traj0), std::move(traj1));
  }

} // namespace corsika
