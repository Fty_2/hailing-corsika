//
// Created by cen on 22-7-12.
//
#include <iostream>
#include <vector>
#include <iomanip>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"


#ifndef HAILING_MYPARTICLETREE_H
#define HAILING_MYPARTICLETREE_H

using std::vector;
typedef struct myParticle {
    int PID;
    int PDG;
    vector<myParticle*> children;
    myParticle* parent;
    double energy;      //energy when born

    double begin_t;
    double begin_px, begin_py, begin_pz;
    double begin_x, begin_y, begin_z;

    double end_t;
    double end_px, end_py, end_pz;
    double end_x, end_y, end_z;

//    ~myParticle(){
//        for(auto p: children)   delete p;
//        delete parent;
//    }
} myParticle;

class MyParticleTree {
public:
    MyParticleTree(){   list_size = 0;    };
    ~MyParticleTree(){
        clearTree();
    }

    void clearTree(){
        list_size = 0;
        for(auto particle: particle_list)   delete particle;
        particle_list.clear();
    }
    myParticle* genMyParticle(int PDG, double energy, double start_t, double end_t,
                              double* start_p, double* end_p, double* start_loc, double* end_loc);

    static void addChild(myParticle* parent, myParticle* child);

    static void setParticleBeginInfo(myParticle* particle, myParticle* parent, int PID, int PDG,
                              double energy, double begin_t, double *begin_p, double *loc);

    static void setParticleEndInfo(myParticle *particle, double end_t, double *end_p, double *loc);

    static void printParticle(myParticle* particle);

    static void transversalPrint(myParticle* root);

    void saveAsRoot(TString fileName = "particleTree.root", TString treeName = "event0", bool recreate = true);

    void transversalPrint(){
        transversalPrint((particle_list[0]));
    }

    int getListSize() const{
        return list_size;
    }

    void contractParticleList();

private:
    myParticle* inverseSearchEndT(double end_t);

    vector<myParticle*> particle_list;
    int list_size;
    constexpr static const double timeResolution = 1e-6, energyResolution = 1e-3, locResolution = 1e-6;

};


#endif //HAILING_MYPARTICLETREE_H
