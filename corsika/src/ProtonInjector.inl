namespace corsika {
ProtonInjector::ProtonInjector(const HEPEnergyType E0,
                          double const thetaRad, double const phiRad):
                          E0_(E0),
                          thetaRad_(thetaRad),
                          phiRad_(phiRad){};


template <typename TStack, typename TEnvironment>
void ProtonInjector::SetupInjection(TStack &stack, TEnvironment &env){
    
//     LengthType const height_atmosphere = 112.8_km;
//     LengthType const detector_depth = 2900_m;   // ~1.6km * sqrt(2) + 0.04km

    CoordinateSystemPtr const& rootCS = env.getCoordinateSystem();

    const Code beamCode = Code::Proton;
    const HEPMassType mass = get_nucleus_mass(beamCode);
    HEPMomentumType P0 = calculate_momentum(E0_, mass);

    // auto momentumComponents = [](double thetaRad, HEPMomentumType ptot) {
    //     return std::make_tuple(ptot * sin(thetaRad), 0_eV, -ptot * cos(thetaRad));
    // };
    
    auto momentumComponents = [](double theta, double phi, HEPMomentumType ptot) {
      return std::make_tuple(ptot * sin(theta) * cos(phi), ptot * sin(theta) * sin(phi),
                             -ptot * cos(theta));
    };
    auto const [px, py, pz] = momentumComponents(thetaRad_,phiRad_ ,P0);

    auto plab = MomentumVector(rootCS, {px, py, pz});
    cout << "input particle: " << beamCode << endl;
    cout << "input angles: theta/rad =" << thetaRad_<< endl;
    cout << "input momentum: " << plab.getComponents() / 1_GeV
         << ", norm = " << plab.getNorm() << endl;
             // setup injection configuration
    auto const observationHeight = -detector_depth + constants::EarthRadius::Mean;
    // auto const observationHeight = -hailing_half_height + constants::EarthRadius::Mean;
    auto const injectionHeight = height_atmosphere + constants::EarthRadius::Mean;
//    auto const injectionHeight = 10.0_km + constants::EarthRadius::Mean;
    auto const t = -observationHeight * cos(thetaRad_) +
                   sqrt(-static_pow<2>(sin(thetaRad_) * observationHeight) +
                        static_pow<2>(injectionHeight));
    Point const showerCore{rootCS, 0_m, 0_m, observationHeight};
    Point const injectionPos = showerCore + DirectionVector{rootCS,
                                   {-sin(thetaRad_) * cos(phiRad_),
                                    -sin(thetaRad_) * sin(phiRad_), cos(thetaRad_)}} *
                        t;

    std::cout << "point of injection: " << injectionPos.getCoordinates() << std::endl;
    std::cout << "point of observation: " << showerCore.getCoordinates() << std::endl;
    std::cout << "Distance to center of the earth: " << sqrt( pow(injectionPos.getX(rootCS)/1_km,2) +
            pow(injectionPos.getY(rootCS)/1_km,2) + pow(injectionPos.getZ(rootCS)/1_km,2)) << endl;

    stack.addParticle(std::make_tuple(
            beamCode, calculate_kinetic_energy(plab.getNorm(), get_mass(beamCode)),
            plab.normalized(), injectionPos, 0_ns));
  

//    return showerAxis;
}
}// namespace corsika