sphinx_rtd_theme >= 0.5.1
sphinx >= 3.5.3
recommonmark >= 0.7.1
breathe >= 4.27.0