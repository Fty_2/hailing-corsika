//
// Created by cen on 22-7-7.
//

#pragma once
#include "RecordParticleProcess.h"

namespace corsika {

    template <typename TOutput>
    RecordParticleProcess<TOutput>::RecordParticleProcess(MyParticleTree* particleTree)
    : particleTree_(particleTree),
    startFlag_(false){
        initializeParticleList();
    }

    template <typename TOutput>
    template <typename TParticle, typename TTrack>
    inline ProcessReturn RecordParticleProcess<TOutput>::doContinuous(TParticle const& particle,
                                                            TTrack const& step, bool const) {
//        if(std::find(recordedParticleList_.begin(), recordedParticleList_.end(), particle.getPDG()) == recordedParticleList_.end())
//            return ProcessReturn::Ok;

        //startFlag is false: start of a step
        if( !startFlag_){
            startFlag_ = true;
            startE_ = particle.getEnergy() / 1_GeV;

        }   //startFlag is true: end of a step
        else if( startFlag_ ){
            auto pdg = static_cast<int>(particle.getPDG());
            endE_ = particle.getEnergy() / 1_GeV;

            auto startT = particle.getTime() / 1_ns;
            auto endT = ( particle.getTime()+ step.getDuration()) / 1_ns;

            HEPEnergyType startPWithUnit = calculate_momentum(startE_ * 1_GeV, particle.getMass());
            DirectionVector startDrct = step.getDirection(0);
            double startP[3] = {
                    (startPWithUnit * startDrct).getComponents().getX() / 1_GeV,
                    (startPWithUnit * startDrct).getComponents().getY() / 1_GeV,
                    (startPWithUnit * startDrct).getComponents().getZ() / 1_GeV,
            };
            HEPEnergyType endPWithUnit = calculate_momentum(endE_ * 1_GeV, particle.getMass());
            DirectionVector endDrct = step.getDirection(1);
            double endP[3] = {
                    (endPWithUnit * endDrct).getComponents().getX() / 1_GeV,
                    (endPWithUnit * endDrct).getComponents().getY() / 1_GeV,
                    (endPWithUnit * endDrct).getComponents().getZ() / 1_GeV,
            };

            double startLoc[3] = {
                    step.getPosition(0).getCoordinates().getX() / 1_km,
                    step.getPosition(0).getCoordinates().getY() / 1_km,
                    step.getPosition(0).getCoordinates().getZ() / 1_km,
            };
            double endLoc[3] = {
                    step.getPosition(1).getCoordinates().getX() / 1_km,
                    step.getPosition(1).getCoordinates().getY() / 1_km,
                    step.getPosition(1).getCoordinates().getZ() / 1_km,
            };

            particleTree_->genMyParticle(pdg, startE_, startT, endT,
                                         startP, endP, startLoc, endLoc);


            startE_ = 0;
            endE_ = 0;
            startFlag_ = false;
        }

//
//        Point const step1 = step.getPosition(1);
//        // Vector const displacement = step1 - plane_.getCenter();
//
//        std::cout << "Particle: "         << std::setw(8) << particle.getPID()
//                  << ". Particle index: " << std::setw(3) << particle.getIndex()
//                  << ". Energy: " << std::setw(16) << energy / 1_GeV
//                  << ". X: " << std::setw(12) << step1.getCoordinates().getX() / 1_m
//                  << ". Y: " << std::setw(12) << step1.getCoordinates().getY() / 1_m
//                  << ". Z: " << std::setw(12) << step1.getCoordinates().getZ() / 1_m
//                  << ". T0 ~ T1: "<< std::setw(8) << particle.getTime() / 1_us
//                  << " ~ " << std::setw(8) <<( particle.getTime()+ step.getDuration()) / 1_us << " (us)"
//                  << std::endl;

        return ProcessReturn::Ok;
    }


    template <typename TOutput>
    template <typename TParticle, typename TTrack>
    inline LengthType RecordParticleProcess<TOutput>::getMaxStepLength(TParticle const&,
                                                             TTrack const&) {
        return meter * std::numeric_limits<double>::infinity();
    }


    template <typename TOutput>
    void RecordParticleProcess<TOutput>::initializeParticleList(){
        recordedParticleList_.emplace_back(PDGCode::Electron); recordedParticleList_.emplace_back(PDGCode::Positron);
        recordedParticleList_.emplace_back(PDGCode::NuE); recordedParticleList_.emplace_back(PDGCode::NuEBar);
        recordedParticleList_.emplace_back(PDGCode::MuMinus); recordedParticleList_.emplace_back(PDGCode::MuPlus);
        recordedParticleList_.emplace_back(PDGCode::NuMu); recordedParticleList_.emplace_back(PDGCode::NuMuBar);
        recordedParticleList_.emplace_back(PDGCode::TauMinus); recordedParticleList_.emplace_back(PDGCode::TauPlus);
        recordedParticleList_.emplace_back(PDGCode::NuTau); recordedParticleList_.emplace_back(PDGCode::NuTauBar);
        recordedParticleList_.emplace_back(PDGCode::Photon);
        recordedParticleList_.emplace_back(PDGCode::K0Long); recordedParticleList_.emplace_back(PDGCode::K0Short);
        recordedParticleList_.emplace_back(PDGCode::PiPlus); recordedParticleList_.emplace_back(PDGCode::PiMinus);
        recordedParticleList_.emplace_back(PDGCode::Pi0);
        recordedParticleList_.emplace_back(PDGCode::K0); recordedParticleList_.emplace_back(PDGCode::K0Bar);
        recordedParticleList_.emplace_back(PDGCode::Proton); recordedParticleList_.emplace_back(PDGCode::AntiProton);
        recordedParticleList_.emplace_back(PDGCode::Neutron); recordedParticleList_.emplace_back(PDGCode::AntiNeutron);
    }


    template <typename TOutput>
    inline YAML::Node RecordParticleProcess<TOutput>::getConfig() const {
        YAML::Node node;
        node["type"] = "BetheBlochPDG";
        return node;
    }

} // namespace corsika