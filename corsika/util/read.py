import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#read
partilces = pd.read_parquet('../build/bin/hailingOutput/particles/particles.parquet')

cut = 50 
#earth
earth_radius = 6371000 #m 

#pdg
pdg_table = {
  'Unknown' : 0,
  'Electron' : 11,
  'Positron' : -11,
  'NuE' : 12,
  'NuEBar' : -12,
  'MuMinus' : 13,
  'MuPlus' : -13,
  'NuMu' : 14,
  'NuMuBar' : -14,
  'TauMinus' : 15,
  'TauPlus' : -15,
  'NuTau' : 16,
  'NuTauBar' : -16,
  'Photon' : 22,
  'Z0' : 23,
  'WPlus' : 24,
  'WMinus' : -24,
  'H0' : 25,
  'Pi0' : 111,
  'Rho0' : 113,
  'K0Long' : 130,
  'PiPlus' : 211,
  'PiMinus' : -211,
  'RhoPlus' : 213,
  'RhoMinus' : -213,
  'Pi1300Plus' : 100211,
  'Pi1300Minus' : -100211,
  'Pi1300_0' : 100111,
  'Eta' : 221,
  'Omega' : 223,
  'K0Short' : 310,
  'K0' : 311,
  'K0Bar' : -311,
  'KStar0' : 313,
  'KStar0Bar' : -313,
  'KStar0_1430_0' : 10311,
  'KStar0_1430_0Bar' : -10311,
  'KStar0_1430_Plus' : 10321,
  'KStar0_1430_MinusBar' : -10321,
  'KPlus' : 321,
  'KMinus' : -321,
  'KStarPlus' : 323,
  'KStarMinus' : -323,
  'EtaPrime' : 331,
  'Phi' : 333,
  'DPlus' : 411,
  'DMinus' : -411,
  'DStarPlus' : 413,
  'DStarMinus' : -413,
  'D0' : 421,
  'D0Bar' : -421,
  'DStar0' : 423,
  'DStar0Bar' : -423,
  'DsPlus' : 431,
  'DsMinus' : -431,
  'DStarSPlus' : 433,
  'DStarSMinus' : -433,
  'EtaC' : 441,
  'Jpsi' : 443,
  'B0' : 511,
  'B0Bar' : -511,
  'BPlus' : 521,
  'BMinus' : -521,
  'Bs0' : 531,
  'Bs0Bar' : -531,
  'BcPlus' : 541,
  'BcMinus' : -541,
  'DeltaMinus' : 1114,
  'DeltaPlusBar' : -1114,
  'Neutron' : 2112,
  'AntiNeutron' : -2112,
  'Delta0' : 2114,
  'Delta0Bar' : -2114,
  'Proton' : 2212,
  'AntiProton' : -2212,
  'N1440Plus' : 202212,
  'N1440MinusBar' : -202212,
  'N1440_0' : 202112,
  'N1440_0Bar' : -202112,
  'N1710Plus' : 212212,
  'N1710MinusBar' : -212212,
  'N1710_0' : 212112,
  'N1710_0Bar' : -212112,
  'DeltaPlus' : 2214,
  'DeltaMinusBar' : -2214,
  'DeltaPlusPlus' : 2224,
  'DeltaMinusMinusBar' : -2224,
  'SigmaMinus' : 3112,
  'SigmaPlusBar' : -3112,
  'SigmaStarMinus' : 3114,
  'SigmaStarPlusBar' : -3114,
  'Lambda0' : 3122,
  'Lambda0Bar' : -3122,
  'Sigma0' : 3212,
  'Sigma0Bar' : -3212,
  'SigmaStar0' : 3214,
  'SigmaStar0Bar' : -3214,
  'SigmaPlus' : 3222,
  'SigmaMinusBar' : -3222,
  'SigmaStarPlus' : 3224,
  'SigmaStarMinusBar' : -3224,
  'XiMinus' : 3312,
  'XiPlusBar' : -3312,
  'XiStarMinus' : 3314,
  'XiStarPlusBar' : -3314,
  'Xi0' : 3322,
  'Xi0Bar' : -3322,
  'XiStar0' : 3324,
  'XiStar0Bar' : -3324,
  'OmegaMinus' : 3334,
  'OmegaPlusBar' : -3334,
  'SigmaC0' : 4112,
  'SigmaC0Bar' : -4112,
  'SigmaStarC0' : 4114,
  'SigmaStarC0Bar' : -4114,
  'LambdaCPlus' : 4122,
  'LambdaCMinusBar' : -4122,
  'XiC0' : 4132,
  'XiC0Bar' : -4132,
  'SigmaCPlus' : 4212,
  'SigmaCMinusBar' : -4212,
  'SigmaStarCPlus' : 4214,
  'SigmaStarCMinusBar' : -4214,
  'SigmaCPlusPlus' : 4222,
  'SigmaCMinusMinusBar' : -4222,
  'SigmaStarCPlusPlus' : 4224,
  'SigmaStarCMinusMinusBar' : -4224,
  'XiCPlus' : 4232,
  'XiCMinusBar' : -4232,
  'XiPrimeC0' : 4312,
  'XiPrimeC0Bar' : -4312,
  'XiStarC0' : 4314,
  'XiStarC0Bar' : -4314,
  'XiPrimeCPlus' : 4322,
  'XiPrimeCMinusBar' : -4322,
  'XiStarCPlus' : 4324,
  'XiStarCMinusBar' : -4324,
  'OmegaC0' : 4332,
  'OmegaC0Bar' : -4332,
  'SigmaBMinus' : 5112,
  'SigmaBPlusBar' : -5112,
  'LambdaB0' : 5122,
  'LambdaB0Bar' : -5122,
  'XiBMinus' : 5132,
  'XiBPlusBar' : -5132,
  'SigmaBPlus' : 5222,
  'SigmaBMinusBar' : -5222,
  'XiB0' : 5232,
  'XiB0Bar' : -5232,
  'OmegaBMinus' : 5332,
  'OmegaBPlusBar' : -5332,
  'Lambda1405_0' : 13122,
  'Lambda1405_0Bar' : -13122,
}

inverse_pdg={}
for key,val in pdg_table.items():
    inverse_pdg[val]=key

########## paticles term ###########
shower = partilces["shower"]
pdg = partilces["pdg"].map(inverse_pdg)
energy = partilces["energy"]
x = partilces["x"]
y = partilces["y"]
z = partilces["z"]
nx = partilces["nx"]
ny = partilces["ny"]
nz = partilces["nz"]
t = partilces["t"]

#plot particle name
plt.figure(figsize=(12, 9), dpi=360)
plt.hist(pdg,log=True) 
plt.ylabel('number ',fontsize=14)

plt.savefig("output/pdg.pdf",dpi=360)

#plot energy distribution
plt.figure(figsize=(8, 6), dpi=360)
plt.hist(energy,bins = 30,weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, label=None, stacked=False) 
plt.xlabel('energy [GeV]',fontsize=14)
plt.ylabel('number',fontsize=14)

plt.savefig("output/energy.pdf",dpi=360)


moun_energy = partilces["energy"][(partilces["pdg"] == 13) | (partilces["pdg"] == -13)]
#plot moun energy distribution
plt.figure(figsize=(8, 6), dpi=360)
plt.hist(moun_energy,bins = 30,weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, label=None, stacked=False) 
plt.xlabel('energy [GeV]',fontsize=14)
plt.ylabel('number',fontsize=14)

plt.savefig("output/moun_energy.pdf",dpi=360)

plt.figure(figsize=(8, 6), dpi=360)
plt.hist(energy,bins = 30,weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, label=None, stacked=False) 
plt.xlabel('energy [GeV]',fontsize=14)
plt.ylabel('number',fontsize=14)

plt.savefig("output/energy.pdf",dpi=360)

#plot z distribution
plt.figure(figsize=(8, 6), dpi=360)
plt.hist(z,bins = 30,weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, label=None, stacked=False) 
plt.xlabel('distance [m]',fontsize=14)
plt.ylabel('number',fontsize=14)

plt.savefig("output/z.pdf",dpi=360)

#plot time distribution
plt.figure(figsize=(8, 6), dpi=360)
plt.hist(t - t.min() ,bins = 30,weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, label=None, stacked=False) 
plt.xlabel('relative time[ns]',fontsize=14)
plt.ylabel('number',fontsize=14)

plt.savefig("output/time.pdf",dpi=360)

#plot moun time distribution

t_moun = partilces["t"][(partilces["pdg"] == 13) | (partilces["pdg"] == -13)]
plt.figure(figsize=(8, 6), dpi=360)
plt.hist(t_moun - t_moun.min() ,bins = 30,weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, label=None, stacked=False) 
plt.xlabel('relative time[ns]',fontsize=14)
plt.ylabel('number',fontsize=14)

plt.savefig("output/moun_time.pdf",dpi=360)

########## track term ###########
track = pd.read_parquet('../build/bin/hailingOutput/tracks/tracks.parquet')

def particle_count(height, track : pd.DataFrame):
    #  height = earth_radius
    return track[(track['start_z'] > height) & (track['end_z'] <= height)]
    
plane_track =  particle_count(0 + earth_radius,track)


def num_height(showerid, pdg, track : pd.DataFrame):

    shower = track[track['shower'] == showerid]

    track_pdg = shower[shower['pdg'] == pdg]
    track_pdg.reset_index(drop=True, inplace=True)
    num = np.array([])
    if (track_pdg['end_z'] == track_pdg['start_z']).all():
        for i in range(len(track_pdg)):
          num = np.append(num,np.arange(track_pdg['end_z'][i], track_pdg['start_z'][i] + 0.5,0.5))
    else:
        for i in range(len(track_pdg)):
            num = np.append(num,np.arange(track_pdg['end_z'][i], track_pdg['start_z'][i] ,0.5))
        
    return num

def plot_astro(showerid,pdg,track):
   num = num_height(showerid,pdg,track)
   plt.hist((num - earth_radius)/1000,bins=np.arange(np.min(num - earth_radius)-0.25, np.max(num - earth_radius) +  0.25 , 0.5)/1000,weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, stacked=False ,label = inverse_pdg[pdg]) 

plt.figure(figsize=(15, 6), dpi=500)

num = np.append(np.append(num_height(0,11,track) ,num_height(0, -11,track)) , num_height(0, 22,track) )
plt.hist((num - earth_radius)/1000,bins=np.arange(np.min(num - earth_radius)-0.25, np.max(num - earth_radius) +  0.25 , 0.5)/1000,weights=None, cumulative=False, bottom=None,     
            histtype='step', align=u'left', orientation=u'vertical',     
          log=True, color="green", stacked=False ,label = "em") 

num = np.append(num_height(0,13,track) ,num_height(0, -13,track))
plt.hist((num - earth_radius)/1000,bins=np.arange(np.min(num - earth_radius)-0.25, np.max(num - earth_radius) +  0.25 , 0.5)/1000,weights=None, cumulative=False, bottom=None,     
            histtype='step', align=u'left', orientation=u'vertical',     
          log=True, color='blue', stacked=False ,label = "mouns") 

num = num_height(0,2112,track)
plt.hist((num - earth_radius)/1000,bins=np.arange(np.min(num - earth_radius)-0.25, np.max(num - earth_radius) +  0.25 , 0.5)/1000,weights=None, cumulative=False, bottom=None,     
            histtype='step', align=u'left', orientation=u'vertical',     
          log=True, color='red', stacked=False ,label = "neutrons") 


num = num_height(0,2212,track)
plt.hist((num - earth_radius)/1000,bins=np.arange(np.min(num - earth_radius)-0.25, np.max(num - earth_radius) +  0.25 , 0.5)/1000,weights=None, cumulative=False, bottom=None,     
            histtype='step', align=u'left', orientation=u'vertical',     
          log=True, color='purple', stacked=False ,label = "protons") 

# plot_astro(0,2112,track)
# plot_astro(0,2212,track)
# plot_astro(0,11,track)
# plot_astro(0,111,track)
# plot_astro(0,211,track)
# plot_astro(0,321,track)
# plot_astro(0,22,track)
# plot_astro(0,2212,track)
# plot_astro(0,2112,track)
plt.xlabel('distance [km]',fontsize=14)
plt.ylabel('number (E>'+str(cut)+'GeV)',fontsize=14)
plt.xlim(-3,50)
plt.legend()
plt.title("1 PeV proton shower in atmosphere ")
plt.savefig("output/particleInastro.pdf",dpi=360)


################ under water ########################

def plot_water(showerid,pdg,track):
   num = num_height(showerid,pdg,track)
   plt.hist((num - earth_radius),bins= np.arange(-2900.25,0.25,0.5),weights=None, cumulative=False, bottom=None,     
                histtype='step', align=u'left', orientation=u'vertical',     
              log=True, color=None, stacked=False ,label = inverse_pdg[pdg]) 

plt.figure(figsize=(8, 6), dpi=500)
plot_water(0,11,track)
plot_water(0,13,track)
plot_water(0,111,track)
plot_water(0,211,track)
plot_water(0,321,track)
plot_water(0,22,track)
plot_water(0,2212,track)
plot_water(0,2112,track)
plt.xlabel('distance [m]',fontsize=14)
plt.ylabel('number',fontsize=14)
plt.legend()
plt.savefig("output/particleInwater.pdf",dpi=360)



# shower = track[track['shower'] == 0]

# track_pdg = shower[shower['pdg'] == 13]

# plt.plot(track_pdg[track_pdg['start_z'] <= earth_radius]['start_z'],track_pdg[track_pdg['start_z'] <= earth_radius]['energy'])