//
// Created by cen on 22-7-12.
//

#include "MyParticleTree.h"
using std::abs;

myParticle* MyParticleTree::genMyParticle(int PDG, double energy, double start_t, double end_t,
                                          double* start_p, double* end_p, double* start_loc, double* end_loc) {

    myParticle* parent = inverseSearchEndT(start_t);

    if(parent!= nullptr&&
       (abs(parent->end_x - start_loc[0]) < locResolution) &&
       (abs(parent->end_y - start_loc[1]) < locResolution) &&
       (abs(parent->end_z - start_loc[2]) < locResolution)  ) {

        if ((parent->PDG == PDG) &&
            (abs(parent->end_px - start_p[0]) < energyResolution) &&
            (abs(parent->end_py - start_p[1]) < energyResolution) &&
            (abs(parent->end_pz - start_p[2]) < energyResolution)
                ) {  //parent is the particle itself
            setParticleEndInfo(parent, end_t, end_p, end_loc);
            return parent;
        } else {
            auto *particle = new myParticle();
            setParticleBeginInfo(particle, parent, list_size, PDG, energy, start_t, start_p, start_loc);
            setParticleEndInfo(particle, end_t, end_p, end_loc);
            particle_list.emplace_back(particle);
            addChild(parent, (particle_list[list_size]));
            list_size = list_size + 1;
            return (particle_list[list_size - 1]);
        }
    }else  {//Cannot find parent
        if(list_size != 0)  std::cout<<"WARNING FROM MyParticleTree: exists a particle with no parent."
                                       " Particle born time: " << end_t << std::endl;

        auto *particle = new myParticle();
        setParticleBeginInfo(particle, nullptr, list_size, PDG, energy, start_t, start_p, start_loc);
        setParticleEndInfo(particle, end_t, end_p, end_loc);
        particle_list.emplace_back(particle);
        list_size = list_size + 1;
        return (particle_list[list_size - 1]);
    }
}

myParticle* MyParticleTree::inverseSearchEndT(double end_t){
    for(int id = list_size - 1; id >= 0; id--){
        if(abs(end_t-particle_list[id]->end_t)<timeResolution){
            return (particle_list[id]);
        }
    }
    return nullptr;
}


void MyParticleTree::addChild(myParticle *parent, myParticle *child) {
    (parent->children).emplace_back(child);
}


void MyParticleTree::setParticleBeginInfo(myParticle* particle, myParticle* parent, int PID, int PDG,
                                          double energy, double begin_t, double *begin_p, double *loc) {
    particle->parent = parent;
    particle->PID = PID;
    particle->PDG = PDG;
    particle->energy = energy;
    particle->begin_t = begin_t;
    particle->begin_px = begin_p[0]; particle->begin_py = begin_p[1]; particle->begin_pz = begin_p[2];
    particle->begin_x = loc[0]; particle->begin_y = loc[1]; particle->begin_z = loc[2];
}


void MyParticleTree::setParticleEndInfo(myParticle *particle, double end_t, double *end_p, double *loc) {
    particle->end_t = end_t;
    particle->end_px = end_p[0]; particle->end_py = end_p[1]; particle->end_pz = end_p[2];
    particle->end_x = loc[0]; particle->end_y = loc[1]; particle->end_z = loc[2];
}

void MyParticleTree::contractParticleList(){
    for(auto iter=particle_list.begin(); iter!=particle_list.end();){
        auto particle = *iter;
        if((particle->children).size() == 1 &&  particle->PDG == ((particle->children)[0])->PDG ){
            //If parent and child is in a same particle list: transfer the parent information to child and delete parent
            auto child = (particle->children)[0];
            double begin_p[3] = {particle->begin_px,particle->begin_py,particle->begin_pz};
            double begin_loc[3] = {particle->begin_x, particle->begin_y, particle->begin_z};
            setParticleBeginInfo(child, particle->parent, child->PID, child->PDG,
                                 particle->energy, particle->begin_t, begin_p, begin_loc);
            auto parent = particle->parent;
            if(parent != nullptr) {
                auto tmpitr = std::find((parent->children).begin(), (parent->children).end(), particle);
                if (tmpitr != (parent->children).end())
                    (parent->children)[tmpitr - (parent->children).begin()] = child;

            }
            delete particle;
            particle_list.erase(iter);
        }else {
            iter++;
        }
    }
}

void MyParticleTree::saveAsRoot(TString fileName, TString treeName, bool recreate) {
    TFile* file;
    if(recreate)    file = new TFile(fileName, "RECREATE");
    else            file = new TFile(fileName, "UPDATE");
    auto tree = new TTree(treeName, treeName);
    int PID, PDG;
    double begin_t, end_t;
    double begin_loc[3], end_loc[3];
    double begin_p[3], end_p[3];
    int parent;
    std::vector<int> daughters;
    double begin_E, end_E;

    tree->Branch("PID",&PID,"PID/I");
    tree->Branch("PDG",&PDG,"PDG/I");
    tree->Branch("parent",&parent,"parent/I");
    tree->Branch("daughters", &daughters);
    tree->Branch("t_born",&begin_t,"t_born/D");
    tree->Branch("t_dead",&end_t,"t_dead/D");
    tree->Branch("E_born",&begin_E,"E_born/D");
    tree->Branch("begin_loc",begin_loc,"begin_loc[3]/D");
    tree->Branch("begin_p",begin_p,"begin_p[3]/D");
    tree->Branch("E_dead",&end_E,"E_dead/D");
    tree->Branch("end_loc",end_loc,"end_loc[3]/D");
    tree->Branch("end_p",end_p,"end_p[3]/D");

    for(auto &particle: particle_list){
        PID = particle->PID;
        PDG = particle->PDG;

        begin_t = particle->begin_t;    end_t = particle->end_t;
        begin_E = particle->energy;
        end_E = sqrt(pow(begin_E,2)
                - pow(particle->begin_px,2) - pow(particle->begin_py,2) - pow(particle->begin_pz,2)
                + pow(particle->end_px,2) + pow(particle->end_py,2) + pow(particle->end_pz,2));

        parent = ((particle->parent)== nullptr? -1 : (particle->parent)->PID);
        for(auto& child: particle->children)    daughters.emplace_back(child->PID);
        begin_loc[0] = particle->begin_x; begin_loc[1] = particle->begin_y; begin_loc[2] = particle->begin_z;
        end_loc[0] = particle->end_x; end_loc[1] = particle->end_y; end_loc[2] = particle->end_z;

        begin_p[0] = particle->begin_px; begin_p[1] = particle->begin_py; begin_p[2] = particle->begin_pz;
        end_p[0] = particle->end_px; end_p[1] = particle->end_py; end_p[2] = particle->end_pz;

        tree->Fill();
        daughters.clear();
    }
    tree->Write();
    delete tree;
    file->Close();
    delete file;
}


void MyParticleTree::printParticle(myParticle *particle) {
    std::cout << "Particle: "         << std::setw(4) << particle->PDG
              << ". PID: " << std::setw(3) << particle->PID
              << ". Energy: " << particle->energy
              << std::endl
              << "  begin:    X: " << std::setw(10) << particle->begin_x
              << ". Y: " << std::setw(10) << particle->begin_y
              << ". Z: " << std::setw(10) << particle->begin_z
              << ". Px: " << std::setw(10) << particle->begin_px
              << ". py: " << std::setw(10) << particle->begin_py
              << ". pz: " << std::setw(10) << particle->begin_pz
              << ". T: "<<particle->begin_t
              << std::endl
              << "  end:      X: " << std::setw(10) << particle->end_x
              << ". Y: " << std::setw(10) << particle->end_y
              << ". Z: " << std::setw(10) << particle->end_z
              << ". Px: " << std::setw(10) << particle->end_px
              << ". py: " << std::setw(10) << particle->end_py
              << ". pz: " << std::setw(10) << particle->end_pz
              << ". T: "<<particle->end_t
              << std::endl;
}


void MyParticleTree::transversalPrint(myParticle* root){
    vector<myParticle*> queue;
    queue.emplace_back(root);
    while(!queue.empty()){
        printParticle(queue[0]);
        for(auto child: queue[0]->children) queue.push_back(child);
        queue.erase(queue.begin());
    }
}
