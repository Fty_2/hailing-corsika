#
# (c) Copyright 2018 CORSIKA Project, corsika-project@lists.kit.edu
#
# See file AUTHORS for a list of contributors.
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3). See file LICENSE for a full version of
# the license.
#

set (CORSIKA8_VERSION 8.0.0.0)


####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was corsikaConfig.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/" ABSOLUTE)

macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

macro(check_required_components _NAME)
  foreach(comp ${${_NAME}_FIND_COMPONENTS})
    if(NOT ${_NAME}_${comp}_FOUND)
      if(${_NAME}_FIND_REQUIRED_${comp})
        set(${_NAME}_FOUND FALSE)
      endif()
    endif()
  endforeach()
endmacro()

####################################################################################

#+++++++++++++++++++++++++++++
# Setup hardware and infrastructure dependent defines and other setting
#
include (${CMAKE_CURRENT_LIST_DIR}/corsikaDefines.cmake)

#+++++++++++++++++++++++++++
# Options
#
option (WITH_HISTORY "Flag to switch on/off HISTORY" ON)
  
#++++++++++++++++++++++++++++
# General config and flags
#
set (CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_EXTENSIONS OFF)
set (COMPILE_OPTIONS )
set (CMAKE_VERBOSE_MAKEFILE  OFF) 

#+++++++++++++++++++++++++++++
# Setup external dependencies via conan
#
include (${CMAKE_CURRENT_LIST_DIR}/conanbuildinfo.cmake)
conan_basic_setup (TARGETS)

#+++++++++++++++++++++++++++++
# Import Pythia8
# since we always import pythia (ExternalProject_Add) we have to
# import here, too.
#
add_library (C8::ext::pythia8 STATIC IMPORTED GLOBAL)
set_target_properties (
  C8::ext::pythia8 PROPERTIES
  IMPORTED_LOCATION /home/cen/project/hailing/corsika/corsika/install/lib/corsika/libpythia8.a
  IMPORTED_LINK_INTERFACE_LIBRARIES dl
  INTERFACE_INCLUDE_DIRECTORIES /home/cen/project/hailing/corsika/corsika/install/include/corsika_modules
  )
set (Pythia8_FOUND 1)
message (STATUS "Pythia8 at: ")


#++++++++++++++++++++++++++++++
# import CORSIKA8
#     
include ("${CMAKE_CURRENT_LIST_DIR}/corsikaTargets.cmake")
check_required_components (corsika)


#+++++++++++++++++++++++++++++++
# add further definitions / options
#
if (WITH_HISTORY)
  set_property (
    TARGET CORSIKA8::CORSIKA8
    APPEND PROPERTY
    INTERFACE_COMPILE_DEFINITIONS "WITH_HISTORY"
  )
endif (WITH_HISTORY)
  


#+++++++++++++++++++++++++++++++
#
# final summary output
#
include (FeatureSummary)
add_feature_info (HISTORY WITH_HISTORY "Full information on cascade history for particles.")
feature_summary (WHAT ALL)
