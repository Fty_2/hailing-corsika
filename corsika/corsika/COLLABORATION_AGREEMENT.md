# Collaboration agreement

The CORSIKA project very much welcomes all collaboration and
contributions. The aim of the CORSIKA project is to create a
scientific software framework as a fundamental tool for research. The
collaboration agreement and the licensing model are based on the
guidelines layed out by HSF
[[1]](https://hepsoftwarefoundation.org/activities/licensing.html) or
CERN
[[2]](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=2ahUKEwiLqKG00dXdAhUOZFAKHdIwAh4QFjAAegQIARAC&url=https%3A%2F%2Findico.cern.ch%2Fcategory%2F4251%2Fattachments%2F101%2F505%2FOSL-2012-01-Open_Source_Licences_at_CERN-Short_version.pdf&usg=AOvVaw1n4S0PQCSeE6wbdfdhKDqF),
[[3]](http://legal.web.cern.ch/licensing/software), and follow the
examples of other big scientific software projects.

The CORSIKA project consists of the contributions from the scientific
community and individuals in a best effort to deliver the best
possible performance and physics output. 
The MCnet guidelines developed by [www.montecarlonet.org](www.montecarlonet.org) 
are copied in [MCNET_GUIDELINES](MCNET_GUIDELINES) -- they provide a very good 
additional scope that contributors should read and consider. 


## The software license of the CORSIKA project

The license adopted for the CORSIKA project is the explicit copyleft
license GPLv3, as copied in full in the file
[LICENSE](LICENSE). Each source file of the CORSIKA project contains a
short statement of the copyright and this license. Each binary or
source code release of CORSIKA contains the file LICENSE. The
code, documentation and content in the folder [./externals](./externals)
is not integral part of the CORSIKA project and can be based on, or
include, other licenses, which must be compatible with GPLv3. 

The folder [./modules](./modules) contains the code of several
external physics models for your convenience. Please consult the
original model authors and honor their policies and licenses. 
Of course, we have their consent to
distribute their code together with CORSIKA 8. 

Check the content of these folders carefully for details and additional 
license information. It depends on the configuration of
the build system to what extend this code is used to build CORSIKA.


## Contributing

If you want to contribute, you need to read
[the GUIDELINES](CONTRIBUTING.md) and comply with these rules, or help to
improve them. 

