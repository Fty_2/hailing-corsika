#include <corsika/framework/process/InteractionCounter.hpp>
#include <corsika/framework/process/ProcessSequence.hpp>
#include <corsika/framework/process/SwitchProcessSequence.hpp>
#include <corsika/framework/core/Cascade.hpp>
#include <corsika/framework/core/PhysicalUnits.hpp>
#include <corsika/framework/core/Logging.hpp>
#include <corsika/framework/core/EnergyMomentumOperations.hpp>
#include <corsika/framework/random/RNGManager.hpp>
#include <corsika/framework/geometry/Sphere.hpp>
#include <corsika/framework/geometry/Cylinder.hpp>
#include <corsika/framework/utility/CorsikaFenv.hpp>
#include <corsika/framework/process/StackProcess.hpp>


#include <corsika/output/OutputManager.hpp>
#include <corsika/modules/writers/SubWriter.hpp>
#include <corsika/modules/writers/EnergyLossWriter.hpp>

#include <corsika/media/Environment.hpp>
#include <corsika/media/HomogeneousMedium.hpp>
#include <corsika/media/NuclearComposition.hpp>
#include <corsika/media/ShowerAxis.hpp>
#include <corsika/media/MediumPropertyModel.hpp>
#include <corsika/media/UniformMagneticField.hpp>

#include <corsika/setup/SetupStack.hpp>
#include <corsika/setup/SetupTrajectory.hpp>

#include <corsika/modules/BetheBlochPDG.hpp>
#include <corsika/modules/StackInspector.hpp>
#include <corsika/modules/Sibyll.hpp>
#include <corsika/modules/Pythia8.hpp>
#include <corsika/modules/ParticleCut.hpp>
#include <corsika/modules/TrackWriter.hpp>
#include <corsika/media/IMagneticFieldModel.hpp>
#include <corsika/media/LayeredSphericalAtmosphereBuilder.hpp>
#include <corsika/media/CORSIKA7Atmospheres.hpp>
#include <corsika/modules/writers/LongitudinalWriter.hpp>
#include <corsika/modules/LongitudinalProfile.hpp>
#include <corsika/modules/ObservationPlane.hpp>
#include <corsika/modules/PROPOSAL.hpp>
#include <corsika/modules/UrQMD.hpp>
#include <corsika/modules/ObservationVolume.hpp>
#include <corsika/framework/geometry/Cylinder.hpp>

#include "hailingProcess.hpp"
#include "ProtonInjector.hpp"
#include <TTree.h>
#include <TFile.h>
#include "TString.h"
#include "TList.h"

#include "constants.h"
#include "MyParticleTree.h"
#include "RecordParticleProcess.h"
/*
  NOTE, WARNING, ATTENTION

  The .../Random.hpp implement the hooks of external modules to the C8 random
  number generator. It has to occur exactly ONCE per linked
  executable. If you include the header below multiple times and
  link this together, it will fail.
 */
#include <corsika/modules/Random.hpp>
#include <iostream>
#include <limits>


using namespace corsika;
using namespace std;
using EnvironmentInterface = IMediumPropertyModel<IMagneticFieldModel<IMediumModel>>;
using EnvType = Environment<EnvironmentInterface>;
using MyHomogeneousModel =
        MediumPropertyModel<UniformMagneticField<HomogeneousMedium<EnvironmentInterface>>>;
using Particle = setup::Stack<EnvType>::particle_type;

template <typename T>
using MyExtraEnv = MediumPropertyModel<UniformMagneticField<T>>;


void CheckEnv(EnvType *env, CoordinateSystemPtr const & rootCS, LengthType innerR, LengthType outerR, LengthType step = 10_km ){

//    VolumeTreeNode<EnvironmentInterface>* const universe = env->getUniverse().get();
    for (auto r= innerR; r < outerR; r += step) {
        Point const ptest{rootCS, 0_m, 0_m, r};
        EnvironmentInterface const& media_prop =
                env->getUniverse()->getContainingNode(ptest)->getModelProperties();
        MassDensityType const rho = media_prop.getMassDensity(ptest);
        NuclearComposition const& nuc_comp = media_prop.getNuclearComposition();
        std::vector<Code> const& nuclei = nuc_comp.getComponents();
        std::vector<double> const& fractions = nuc_comp.getFractions();
        CORSIKA_LOG_INFO(
                "radius: {:.2f} m, density: {:.2f} g/cm^3, nuclei: ({:d}, {:d}), fractions: "
                "({:.2f}, "
                "{:.2f})",
                r / 1_m, rho / (1_g / static_pow<3>(1_cm)), get_PDG(nuclei.at(0)),
                get_PDG(nuclei.at(1)), fractions.at(0), fractions.at(1));
    }
}

// initialize random number sequence(s)
void registerRandomStreams(int seed) {
    RNGManager<>::getInstance().registerRandomStream("cascade");
    RNGManager<>::getInstance().registerRandomStream("qgsjet");
    RNGManager<>::getInstance().registerRandomStream("sibyll");
    RNGManager<>::getInstance().registerRandomStream("epos");
    RNGManager<>::getInstance().registerRandomStream("pythia");
    RNGManager<>::getInstance().registerRandomStream("urqmd");
    RNGManager<>::getInstance().registerRandomStream("proposal");

    if (seed == 0) {
        std::random_device rd;
        seed = rd();
        CORSIKA_LOG_INFO("random seed (auto) {} ", seed);
    } else {
        CORSIKA_LOG_INFO("random seed {} ", seed);
    }
    RNGManager<>::getInstance().setSeed(seed);
}

// setup environment, geometry
void createEnvironment(EnvType &env){
    CoordinateSystemPtr const& rootCS = env.getCoordinateSystem();
    Point const center{rootCS, 0_m, 0_m, 0_m};

    //******* current geometry and atomphere model *****************
    // build a Linsley US Standard atmosphere into `env`

    create_5layer_atmosphere<EnvironmentInterface, MyExtraEnv>(
    env, AtmosphereId::LinsleyUSStd, center, Medium::AirDry1Atm,
    MagneticFieldVector{rootCS, 0_T, 0_T, 0_T});
    
    ofstream atmout("earth.dat");
    for (LengthType h = 0_m; h < 110_km; h += 100_m) {
        Point const ptest{rootCS, 0_m, 0_m, constants::EarthRadius::Mean + h};
        auto rho =
            env.getUniverse()->getContainingNode(ptest)->getModelProperties().getMassDensity(
                ptest);
        atmout << h / 1_m << " " << rho / 1_kg * cube(1_m) << "\n";
    }
    atmout.close();

    auto& universe = *(env.getUniverse());

    auto water = EnvType::createNode<Sphere>(Point{rootCS, 0_m, 0_m, 0_m}, constants::EarthRadius::Mean);
    water->setModelProperties<MyHomogeneousModel>(
            Medium::WaterLiquid, MagneticFieldVector(rootCS, 0_T, 0_T, 0_T),
            1_g / (1_cm * 1_cm * 1_cm),
            NuclearComposition({Code::Hydrogen, Code::Oxygen}, {0.11, 0.89}));

    universe.addChild(std::move(water));

    //******* old geometry and atomphere model *****************

    // auto world = EnvType::createNode<Sphere>(Point{rootCS, 0_m, 0_m, 0_m},
    //                                          constants::EarthRadius::Mean + height_atmosphere + 1_km);
    // cout << "World radius: " << (constants::EarthRadius::Mean + height_atmosphere + 1_km) / 1_km << " km" << endl;
    
    // // fraction of oxygen
    // double const fox = 0.20946;
    // auto const props = world->setModelProperties<MyHomogeneousModel>(
    //         Medium::AirDry1Atm, MagneticFieldVector(rootCS, 0_T, 0_T, 0_T),
    //         1_kg / (1_m * 1_m * 1_m),
    //         NuclearComposition({Code::Nitrogen, Code::Oxygen}, {1. - fox, fox}));
    // world->addChild(std::move(water));
    // universe.addChild(std::move(world));
}

int main(int argc, char** argv) {

    logging::set_level(logging::level::info);
    
    CORSIKA_LOG_INFO("hailing");

    if (argc < 4) {
        std::cerr << "usage: hailing <energy/GeV> <theta/Deg> <nevent> "
                     "<outputDirName>(=\"hailingOutput\") <ecut/GeV>(=50) randomSeed(=0)" << std::endl;
        return 1;
    }

    double initialEnergy = std::stof(std::string(argv[1]));
    double theta = std::stof(std::string(argv[2]));
    int nevent = std::stoi(std::string(argv[3]));
    std::string outputDirName = "hailingOutput";
    HEPEnergyType ecut = 50_GeV;
    int randomSeed = 0;
    int MCTESTflag = 0;
    if (argc > 4) outputDirName = std::string(argv[4]);
    if (argc > 5) ecut = std::stof(std::string(argv[5])) * 1_GeV;
    if (argc > 6) randomSeed =  std::stoi(std::string(argv[6]));
    if (argc > 7) MCTESTflag =  std::stoi(std::string(argv[7]));

    feenableexcept(FE_INVALID);

    // setup environment, geometry
    EnvType env;
    CoordinateSystemPtr const& rootCS = env.getCoordinateSystem();
    createEnvironment(env);
    bool checkEnvFlag = false;
    if( checkEnvFlag ){
        CheckEnv( &env, rootCS, 0_km, constants::EarthRadius::Mean + height_atmosphere, 10_km);
    }

    // initialize random number sequence(s)
    registerRandomStreams(randomSeed);

    OutputManager output(outputDirName);
    setup::Tracking tracking;

    TrackWriter trackWriter;
    output.add("tracks", trackWriter); // register TrackWriter

    // setup particle stack, and add primary particle
    setup::Stack<EnvType> stack;
    stack.clear();
    const HEPEnergyType E0 = 1_GeV * initialEnergy;

    auto const thetaRad = theta / 180. * M_PI;
    auto const phiRad = 0. / 180. * M_PI ;
     

    //************ interaction model ***********************
    
    // * first version 

    // ParticleCut cut(ecut, true);
    // corsika::sibyll::Interaction sibyll;
    // corsika::sibyll::NuclearInteraction sibyllNuc{sibyll, env};
    // auto heModel = make_sequence(sibyllNuc, sibyll);
    // corsika::sibyll::Decay decay;
    // HEPEnergyType heThresholdNN = 80_GeV;
    // BetheBlochPDG emContinuous;
    // // total physics list
    // auto physics_sequence = make_sequence(
    //                 // emCascade,
    //                 emContinuous,
    //                 heModel,
    //                 decay
    //                 );
    
    // * second version

    // HEPEnergyType const emcut = 50_GeV;
    // HEPEnergyType const hadcut = 50_GeV;
    // ParticleCut cut(emcut, emcut, hadcut, hadcut, true);

    // HEPEnergyType heHadronModelThreshold = 63.1_GeV;
    // corsika::sibyll::Interaction sibyll;
    // corsika::sibyll::NuclearInteraction sibyllNuc(sibyll, env);
    // auto heModel = make_sequence(sibyllNuc, sibyll);

    // corsika::pythia8::Decay decayPythia; 
    // corsika::sibyll::Decay decaySibyll{{
    //   Code::N1440Plus,
    //   Code::N1440MinusBar,
    //   Code::N1440_0,
    //   Code::N1440_0Bar,
    //   Code::N1710Plus,
    //   Code::N1710MinusBar,
    //   Code::N1710_0,
    //   Code::N1710_0Bar,
    //   Code::Pi1300Plus,
    //   Code::Pi1300Minus,
    //   Code::Pi1300_0,
    //   Code::KStar0_1430_0,
    //   Code::KStar0_1430_0Bar,
    //   Code::KStar0_1430_Plus,
    //   Code::KStar0_1430_MinusBar,
    //  }};
    // auto decaySequence = make_sequence(decayPythia, decaySibyll);

    // corsika::proposal::Interaction emCascade(env, sibyll, heHadronModelThreshold);
    // BetheBlochPDG emContinuous;

    // auto physics_sequence = make_sequence(
    //                 emCascade,
    //                 emContinuous,
    //                 heModel,
    //                 decaySequence
    //                 );

    // * third version

    //  corsika::pythia8::Interaction pythia;
    //  corsika::pythia8::Decay decay;

    //  HEPEnergyType const energycut = 60_GeV;
    //  ParticleCut cut(energycut, true);
    //  BetheBlochPDG emContinuous;
    //  auto physics_sequence = make_sequence(
    //                 pythia,
    //                 decay,
    //                 emContinuous
    //                 );

    // * forth

    //   corsika::sibyll::Interaction sibyll;
    //   corsika::sibyll::NuclearInteraction sibyllNuc(sibyll, env);
    //   corsika::sibyll::Decay decay;

    //   HEPEnergyType const energycut = 60_GeV;
    //   ParticleCut cut(energycut, true);
    //   BetheBlochPDG emContinuous;
    //   auto physics_sequence = make_sequence(
    //                     sibyll,
    //                     sibyllNuc,
    //                     decay,
    //                     emContinuous
    //                     );
    
    // * fifth 

    corsika::sibyll::Interaction sibyll;
    InteractionCounter sibyllCounted(sibyll);
    corsika::sibyll::NuclearInteraction sibyllNuc(sibyll, env);
    InteractionCounter sibyllNucCounted(sibyllNuc);
    auto heModelCounted = make_sequence(sibyllNucCounted, sibyllCounted);

    corsika::pythia8::Decay decayPythia;

    // use sibyll decay routine for decays of particles unknown to pythia
    corsika::sibyll::Decay decaySibyll{{
        Code::N1440Plus,
        Code::N1440MinusBar,
        Code::N1440_0,
        Code::N1440_0Bar,
        Code::N1710Plus,
        Code::N1710MinusBar,
        Code::N1710_0,
        Code::N1710_0Bar,

        Code::Pi1300Plus,
        Code::Pi1300Minus,
        Code::Pi1300_0,

        Code::KStar0_1430_0,
        Code::KStar0_1430_0Bar,
        Code::KStar0_1430_Plus,
        Code::KStar0_1430_MinusBar,
    }};
    HEPEnergyType const emcut = ecut;
    HEPEnergyType const hadcut = ecut;
    ParticleCut cut(emcut, emcut, hadcut, hadcut, true);

    // energy threshold for high energy hadronic model. Affects LE/HE switch for hadron
    // interactions and the hadronic photon model in proposal
    HEPEnergyType heHadronModelThreshold = 63.1_GeV;
    corsika::proposal::Interaction emCascade(env, sibyll, heHadronModelThreshold);
    BetheBlochPDG emContinuous;

    corsika::urqmd::UrQMD urqmd;
    InteractionCounter urqmdCounted(urqmd);

    // assemble all processes into an ordered process list
    struct EnergySwitch {
        HEPEnergyType cutE_;
        EnergySwitch(HEPEnergyType cutE)
            : cutE_(cutE) {}
        bool operator()(const Particle& p) const { return (p.getKineticEnergy() < cutE_); }
    };
    auto hadronSequence =
        make_select(EnergySwitch(heHadronModelThreshold), urqmdCounted, heModelCounted);
    auto decaySequence = make_sequence(decayPythia, decaySibyll);

    // * detector observer without root output

    // Point const detectorCenter =
    //     Point(rootCS, {0_m, 0_m, constants::EarthRadius::Mean - detector_depth});
    // auto const &detCS = make_translation(rootCS, detectorCenter.getCoordinates());
    // Cylinder cylinder(detCS, detector_radius, detector_dz);
//     ObservationVolume<tracking_line::Tracking, Cylinder> hailing(detCS, cylinder, true);

    // output.add("particles", hailing);
    
    // * partilce tree
    auto myParticleTree = new MyParticleTree();
    RecordParticleProcess record(myParticleTree);

    //************ setup output rooot file *******************
     auto* rootFile = new TFile((outputDirName + "/detected_particles.root").c_str(),"RECREATE");
     TTree* tree = nullptr;
     
    // * detector observer with root output
    Point const detectorCenter =
        Point(rootCS, {0_m, 0_m, constants::EarthRadius::Mean - detector_depth});
//    auto const &detCS = make_translation(rootCS, detectorCenter.getCoordinates());
//    Cylinder cylinder(detCS, detector_radius, detector_dz);
//    hailingProcess<tracking_line::Tracking,Cylinder> hailing(detCS,cylinder,true);
    hailingProcess<tracking_line::Tracking> hailing(rootCS);
    output.add("particles", hailing);

    // * assemble all processes into an ordered process list
    // * sequence without record
    // auto sequence = make_sequence(hadronSequence, decaySequence, cut,
    //                             emCascade, emContinuous, trackWriter,
    //                             hailing);
   
    // * sequence with record
    auto sequence = make_sequence(
                                record,
                                hadronSequence, decaySequence, cut,
                                emCascade, emContinuous, trackWriter
//                                ,hailing
                                ,record
                                );


    // define air shower object, run simulation
    Cascade EAS(env, tracking, sequence, output, stack);
    output.startOfLibrary();

    for(int ievent=0; ievent<nevent; ievent++) {
        stack.clear();
        ProtonInjector injector(E0,thetaRad,phiRad);
        injector.SetupInjection(stack,env);
        printf("Event %d begins: \n", ievent + 1);
//        registerRandomStreams(42);
        EAS.run();

        // * root output
//        myParticleTree->contractParticleList();
        if(ievent==0) myParticleTree->saveAsRoot((outputDirName+"/particles.root").c_str(),("event"+ to_string(ievent)).c_str(),true);
        else myParticleTree->saveAsRoot((outputDirName+"/particles.root").c_str(),("event"+ to_string(ievent)).c_str(),false);
        myParticleTree->clearTree();

        printf("Event : %d / %d completed. Initial energy (GeV): %f. Inject direction theta (Rad): %f. \n \n",
               ievent + 1, nevent, initialEnergy, thetaRad);
    }
    output.endOfLibrary();
    
    // * root output
    tree = hailing.getOutputTree();
    rootFile->cd();
    tree->Write();
    rootFile->Close();

    return 0;
}