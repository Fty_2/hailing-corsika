#include <iomanip>
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TString.h"
#include "iostream"
#include "vector"
#include "TCanvas.h"
#include "map"
#include "TLegend.h"
#include "TStyle.h"
//#include <iomanip>

using namespace std;

Color_t const ColorOption[6] = {kOrange, kPink, kViolet, kAzure, kTeal, kYellow+3};

void setHistStyle(TH1F* h, Color_t lineColor=kBlack, Width_t lineWidth = 2, Style_t lineStyle = -1,
                  TString xAxisTitle = "NOCHANGE", TString yAxisTitle = "NOCHANGE",
                  double xmin=-999, double xmax=-999, double ymin=-999, double ymax=-999,
                  TString histTitle="NOCHANGE"
                  ){
    h->SetLineColor(lineColor);
    h->SetLineWidth(lineWidth);
    if(xAxisTitle!="NOCHANGE")  h->SetXTitle(xAxisTitle);
    if(yAxisTitle!="NOCHANGE") h->SetYTitle(yAxisTitle);
    if(lineStyle!=-1) h->SetLineStyle(lineStyle);
    if(xmin!=-999 || xmax!=-999) h->GetXaxis()->SetRangeUser(xmin,xmax);
    if(ymin!=-999 || ymax!=-999) h->GetYaxis()->SetRangeUser(ymin,ymax);
    if(histTitle!="NOCHANGE") h->SetTitle(histTitle);
}

void drawHist(vector<TTree*>& treeList, vector<TString>& fileNameList,vector<TString>& branchNameList,
              vector<double>& branchMin, vector<double>& branchMax, vector<TString>& branchUnit){
    auto c = new TCanvas("c"), c0 = new TCanvas("c0");
    c->SetLogy();
    vector<TH1F*> hists;

    int ibranch = 0;
    for(auto &branchName: branchNameList){
        TLegend leg(.55,.7,.8,.9);
        leg.SetFillColor(0);

        for(int itree=0; itree<treeList.size(); itree++){
            c0->cd();
            TString histName = "h_" + TString(to_string(ibranch))+"_" + TString(to_string(itree));
            hists.emplace_back( new TH1F(histName, histName, 100, branchMin[ibranch], branchMax[ibranch]) );
            treeList[itree]->Draw(branchName+">>"+histName);

//            hists.emplace_back( (TH1F*)gDirectory->Get(histName ) );
            setHistStyle(hists[itree], ColorOption[itree],2, 1,
                         branchUnit[ibranch],"num",-999,-999,0.5,1e5, branchName);
            leg.AddEntry(hists[itree], fileNameList[itree]);
            c->cd();
            if(itree==0) hists[itree]->Draw("hist");
            else    hists[itree]->Draw("SAME hist");
        }
        leg.Draw("SAME");
        c->SaveAs(branchName+".png");
        c->Clear();
        hists.clear();
        ibranch++;
    }
    c->Close();
    c0->Close();
}

void initializePDGHist(TH1F* h){
    h->Fill("22",0);
    h->Fill("-2212",0); h->Fill("-2112",0); h->Fill("-13",0); h->Fill("-11",0);
    h->Fill("11",0); h->Fill("13",0); h->Fill("2112",0); h->Fill("2212",0);

    h->SetXTitle("PDG");
    h->SetYTitle("Num");
    h->GetXaxis()->SetRangeUser(0,9);
}

void printPDGInformation(vector<TTree*>& treeList, vector<TString>& fileNameList){
    map<int, int> allPDGNum;
    auto allH = new TH1F();
    setHistStyle(allH);
    initializePDGHist(allH); allH->GetYaxis()->SetRangeUser(0.5, 1e5);

    TLegend leg(.7,.7,.9,.9);
    leg.SetFillColor(0);
    auto c = new TCanvas();
    c->SetLogy();

    int iTree = 0;
    for(auto &tree : treeList){
        auto nParticle = tree->GetEntries();
        int pdg=0;
        tree->SetBranchAddress("PDG",&pdg);
        map<int, int> pdgNum;
        auto h = new TH1F();
        initializePDGHist(h);
        leg.AddEntry(h, fileNameList[iTree]);

        for(int iEntry=0; iEntry<nParticle; iEntry++){
            tree->GetEntry(iEntry);
            h->Fill( TString(to_string(pdg) ), 1);
            allH->Fill( TString(to_string(pdg) ), 1);
            ++pdgNum[pdg];
            ++allPDGNum[pdg];
        }
        cout<<"In file "<<fileNameList[iTree]<<". Particle Number:"<<nParticle<<endl;
        for(auto &PDGitr: pdgNum){
            cout<<"\tParticle: "<<setw(5)<<PDGitr.first<<". Num: "<<PDGitr.second<<endl;
        }
        cout<<endl;

        setHistStyle(h, ColorOption[iTree]); h->GetYaxis()->SetRangeUser(0.5, 1e5);
        h->Draw("SAME hist");
        iTree++;
    }
    cout<<"For all files:"<<endl;
    for(auto &PDGitr: allPDGNum){
        cout<<"\tParticle: "<<setw(5)<<PDGitr.first<<". Num: "<<PDGitr.second<<endl;
    }
    leg.Draw("SAME");
    c->SaveAs("pdg_separatly.png");
    c->Clear();
    allH->Draw("hist");
    c->SaveAs("pdg_total.png");
    c->Close();
}


void analysis(){
    gStyle->SetOptStat(0);
    TString filePrefix = "/home/cen/project/hailing/workdir/corsika/events/";
    TString fileSuffix = "/detected_particles.root";
    vector<TString> fileNameList;
    fileNameList.emplace_back("1e4GeV0Deg");    fileNameList.emplace_back("1e4GeV10Deg");
    fileNameList.emplace_back("1e4GeV90Deg");   fileNameList.emplace_back("1e4GeV180Deg");
//    fileNameList.emplace_back("1e4GeV45Deg");   fileNameList.emplace_back("1e4GeV135Deg");
    vector<TTree*> treeList;
    for(auto &fileName: fileNameList){
        auto tmp = new TFile(filePrefix + fileName + fileSuffix);
        treeList.emplace_back( (TTree*) tmp->Get("events"));
    }

    //Draw branches and their option.
    vector<TString> branchToDraw;   vector<double> branchMin;   vector<double> branchMax;   vector<TString> branchUnit;
    branchToDraw.emplace_back("Energy"); branchMin.emplace_back(0), branchMax.emplace_back(7000);   branchUnit.emplace_back("GeV");
    branchToDraw.emplace_back("Time"); branchMin.emplace_back(0), branchMax.emplace_back(50);   branchUnit.emplace_back("ms");
    branchToDraw.emplace_back("Px"); branchMin.emplace_back(-2000), branchMax.emplace_back(50);   branchUnit.emplace_back("GeV");
    branchToDraw.emplace_back("Py"); branchMin.emplace_back(-2), branchMax.emplace_back(2);   branchUnit.emplace_back("GeV");
    branchToDraw.emplace_back("Pz"); branchMin.emplace_back(-6000), branchMax.emplace_back(6000);   branchUnit.emplace_back("GeV");
    branchToDraw.emplace_back("X"); branchMin.emplace_back(-2500), branchMax.emplace_back(2500);   branchUnit.emplace_back("m");
    branchToDraw.emplace_back("Y"); branchMin.emplace_back(-2500), branchMax.emplace_back(2500);   branchUnit.emplace_back("m");
    branchToDraw.emplace_back("Z"); branchMin.emplace_back(6366300), branchMax.emplace_back(6371100);   branchUnit.emplace_back("m");

    drawHist(treeList, fileNameList, branchToDraw, branchMin, branchMax, branchUnit);
    printPDGInformation(treeList,fileNameList);
}


int main(int argc, char **argv){
    analysis();
    return 1;
}