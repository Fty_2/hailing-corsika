# CORSIKA 8 - Python Library

To install this into your global environment using `pip` (not recommended), run

``` shell
pip install --user git+https://gitlab.iap.kit.edu/AirShowerPhysics/corsika/-/tree/master/python 
```

