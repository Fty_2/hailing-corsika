/*
 * (c) Copyright 2022 CORSIKA Project, corsika-project@lists.kit.edu
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3). See file LICENSE for a full version of
 * the license.
 */

namespace corsika {
inline bool Cylinder::contains(Point const &p) const {
  auto displace = p - center_;
  auto dz = displace.getZ(cs_);
  auto dr2 = displace.dot(displace) - dz * dz;
  if (dr2 < radius_ * radius_ && abs(dz) < z_)
    return true;
  else
    return false;
}

inline std::string Cylinder::asString() const {
  std::ostringstream txt;
  txt << "center=" << center_ << ", x-axis=" << DirectionVector{cs_, {1, 0, 0}}
      << ", y-axis: " << DirectionVector{cs_, {0, 1, 0}}
      << ", z-axis: " << DirectionVector{cs_, {0, 0, 1}};
  return txt.str();
}

template <typename TDim>
inline void Cylinder::rotate(QuantityVector<TDim> const &axis,
                             double const angle) {
  cs_ = make_rotation(cs_, axis, angle);
}
} // namespace corsika