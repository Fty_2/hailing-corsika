#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "CORSIKA8::cnpy" for configuration "Release"
set_property(TARGET CORSIKA8::cnpy APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::cnpy PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/corsika_external/cnpy/lib/libcnpy.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::cnpy )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::cnpy "${_IMPORT_PREFIX}/corsika_external/cnpy/lib/libcnpy.a" )

# Import target "CORSIKA8::CorsikaData" for configuration "Release"
set_property(TARGET CORSIKA8::CorsikaData APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::CorsikaData PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/corsika/libCorsikaData.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::CorsikaData )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::CorsikaData "${_IMPORT_PREFIX}/lib/corsika/libCorsikaData.a" )

# Import target "CORSIKA8::Sibyll_static" for configuration "Release"
set_property(TARGET CORSIKA8::Sibyll_static APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::Sibyll_static PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX;Fortran"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/corsika/libSibyll_static.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::Sibyll_static )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::Sibyll_static "${_IMPORT_PREFIX}/lib/corsika/libSibyll_static.a" )

# Import target "CORSIKA8::Sibyll" for configuration "Release"
set_property(TARGET CORSIKA8::Sibyll APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::Sibyll PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/corsika/libSibyll.so"
  IMPORTED_SONAME_RELEASE "libSibyll.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::Sibyll )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::Sibyll "${_IMPORT_PREFIX}/lib/corsika/libSibyll.so" )

# Import target "CORSIKA8::QGSJetII_static" for configuration "Release"
set_property(TARGET CORSIKA8::QGSJetII_static APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::QGSJetII_static PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX;Fortran"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/corsika/libQGSJetII_static.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::QGSJetII_static )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::QGSJetII_static "${_IMPORT_PREFIX}/lib/corsika/libQGSJetII_static.a" )

# Import target "CORSIKA8::UrQMD_static" for configuration "Release"
set_property(TARGET CORSIKA8::UrQMD_static APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::UrQMD_static PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX;Fortran"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/corsika/libUrQMD_static.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::UrQMD_static )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::UrQMD_static "${_IMPORT_PREFIX}/lib/corsika/libUrQMD_static.a" )

# Import target "CORSIKA8::CONEXsibyll" for configuration "Release"
set_property(TARGET CORSIKA8::CONEXsibyll APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::CONEXsibyll PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX;Fortran"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/corsika/libCONEXsibyll.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::CONEXsibyll )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::CONEXsibyll "${_IMPORT_PREFIX}/lib/corsika/libCONEXsibyll.a" )

# Import target "CORSIKA8::EPOS_static" for configuration "Release"
set_property(TARGET CORSIKA8::EPOS_static APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(CORSIKA8::EPOS_static PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX;Fortran"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/corsika/libEPOS_static.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS CORSIKA8::EPOS_static )
list(APPEND _IMPORT_CHECK_FILES_FOR_CORSIKA8::EPOS_static "${_IMPORT_PREFIX}/lib/corsika/libEPOS_static.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
