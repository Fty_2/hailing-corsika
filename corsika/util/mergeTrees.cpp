#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TString.h"
#include "TList.h"
#include "vector"

using namespace std;
void mergeTrees(vector<TString>& inputFileNameList, TString outputFileName, TString outputTreeName){
    TFile* infile;
    TList* list = new TList;
    for(auto &infileName: inputFileNameList){
        infile = new TFile(infileName);
        list->Add((TTree *) infile->Get("all_events-"));
    }

    TFile* outfile = new TFile(outputFileName, "RECREATE");
    TTree *mergedTree = TTree::MergeTrees(list);
    mergedTree->SetNameTitle(outputTreeName, outputTreeName);
    outfile->cd();
    mergedTree->Write();
    outfile->Close();
}

int main(int argc, char** argv){
    vector<TString> inputFileList;
    inputFileList.push_back("detected_particles.root");
    mergeTrees(inputFileList, "test.root", "events");
    return 1;
}