//
// Created by cen on 22-7-15.
//

#ifndef HAILING_CONSTANTS_H
#define HAILING_CONSTANTS_H

#include <corsika/framework/core/PhysicalUnits.hpp>

using namespace corsika;

LengthType const height_atmosphere = 111.75_km ;


LengthType detector_depth = 2900_m; // 400m above sea bed
LengthType detector_radius = 2100_m;
LengthType detector_dx = detector_radius;
LengthType detector_dy = detector_radius;
LengthType detector_dz = 400_m;


#endif //HAILING_CONSTANTS_H
